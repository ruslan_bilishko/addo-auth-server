from django.contrib.auth.models import User
from django.contrib import auth
from django.urls import reverse
from django.views.generic import FormView
from oauth2_provider.models import AccessToken
from rest_framework import viewsets

from accounts.serializers import UserSerializer
from accounts.forms import CustomRegistrationForm


class UserViewSet(viewsets.ModelViewSet):
    queryset = User.objects.all()
    serializer_class = UserSerializer
    model = User

    def filter_queryset(self, queryset):
        access_token = self.request.GET.get("token")
        uid = AccessToken.objects.get(token=access_token).user_id
        queryset = queryset.filter(pk=uid)
        return queryset


class RegisterFormView(FormView):
    form_class = CustomRegistrationForm
    success_url = '/accounts/login'
    template_name = "registration/sign_up.html"

    def form_valid(self, form):
        form.save()
        return super(RegisterFormView, self).form_valid(form)
