from django.conf.urls import url, include
from rest_framework import routers

from accounts import views

router = routers.DefaultRouter()
router.register(r'api/user', views.UserViewSet)

urlpatterns = [
    url(r'^accounts/', include('django.contrib.auth.urls')),
    url(r'^accounts/sign-up', views.RegisterFormView.as_view(), name='sign_up'),
    url(r'^o/', include('oauth2_provider.urls', namespace='oauth2_provider')),
    url(r'^', include(router.urls)),
]
