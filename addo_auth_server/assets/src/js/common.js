$(document).ready(function () {

    var win = $(window);
    var winW = win.width(),
        winH = win.height();

    /*----------------------
     PRELOADER
     -----------------------*/
    $(window).load(function () {
        $(".preloader").delay(1000).fadeOut("slow");
        $('body').css('overflow', 'auto');
    });


    /*------------------------------
     owlCarousel
     -------------------------------*/
    $('.companies-logo-slider').owlCarousel({
        autoplay: true,
        loop: true,
        margin: 10,
        nav: false,
        dots: true,
        responsive: {
            0: {
                items: 2
            },
            600: {
                items: 3
            },
            1000: {
                items: 5
            }
        }
    });

    $('.personal-owl-carousel').owlCarousel({
        autoplay: true,
        loop: true,
        margin: 10,
        dots: true,
        smartSpeed: 2000,
        slideSpeed: 8000,
        responsive: {
            0: {
                items: 1
            },
            600: {
                items: 1
            },
            1000: {
                items: 1
            }
        }

    });


    /* -----------------------------
     Magnific Popup Lightbox
     ----------------------------- */
    var video = document.getElementById("video-player");
    $(".video-popup").magnificPopup({
        disableOn: 700,
        removalDelay: 160,
        preloader: false,
        type: 'inline',
        midClick: true,
        mainClass: 'about-video-popup',
        callbacks: {
            open: function () {
                video.currentTime = 0;
                video.play();
            },
            close: function () {
                video.pause();
            }
        }
    });

    /* -----------------------------
     Stellar Parallax
     ----------------------------- */

    win.stellar({
        horizontalScrolling: false,
        verticalOffset: 0
    });
    // fix for resizing bg pos issues
    win.resize(function () {
        $(this).stellar('refresh');
    });
    // fix random pos bug
    win.stellar('refresh');
    setTimeout(function () {
        win.stellar('refresh');
    }, 2000);


    /**
     * Popup messages
     */
    var messages = $(".messages .messages__list.animated");

    setTimeout(function () {
        messages.removeClass('fadeInDown');
        messages.addClass('fadeOutUp');
    }, 3000);

    setTimeout(function () {
        messages.removeClass('animated fadeOutUp');
        messages.hide();

    }, 4000);

    /**
     * Table HOVER
     */

    // $('td').hover(function () {
    //         var t = parseInt($(this).index()) + 1;
    //         $('td:nth-child(' + t + ')').addClass('table-col-hover');
    //     },
    //     function () {
    //         var t = parseInt($(this).index()) + 1;
    //         $('td:nth-child(' + t + ')').removeClass('table-col-hover');
    //     });


    /* -----------------------------
     NoIndex Links
     ----------------------------- */
    $('a.noindex-link').click(function () {
        window.open($(this).attr("rel"));
    });

});
