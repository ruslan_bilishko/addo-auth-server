window.onload = function () {
    if (location.hash) {
        window.scrollTo(0, 0);
        setTimeout(function () {
            $("html, body").animate({
                scrollTop: $(location.hash).offset().top - 50
            }, 1000, 'easeInOutSine');
        }, 1);
    }
}

$(document).ready(function () {

    var win = $(window);
    var winWidth = win.width();
    var winHeight = win.height();
    var main_menu = $("#main-menu");
    var menuContainer = $("#menu-container");
    var mainMenuDefaultHeight = main_menu.height();
    var maxWinWidth = 922;
    var mobileMenuSelector = "mobile_menu";
    var openMenu = $("#open-menu");
    var closeMenu = $("#close-menu");
    var header = $("#header");
    var headerHeidth = header.height();
    var firstSectionHeight = $("#home").height();
    var scrollCorrection = 60;

    function toggleMenu(menu, action) {

        if (action === 'open') {
            menu.addClass('open');

        }

        if (action === 'close') {
            menu.removeClass('open');

        }
    }

    function mobile_menu_handler(container, width, height) {

        if (width <= maxWinWidth) {

            container
                .addClass("mobile_menu")
                .height(height);

            openMenu.on('click', function () {
                toggleMenu(menuContainer, 'open');
            });

            closeMenu.on("click", function () {
                toggleMenu(menuContainer, 'close');
            });

            $("#main-menu a").on("click", function () {

                if (!$(this).hasClass('dropdown-toggle'))
                    toggleMenu(menuContainer, 'close');
            });

        }
        else {
            menuContainer
                .removeClass(mobileMenuSelector)
                .height(mainMenuDefaultHeight);
        }
    }

    function fixedMenuHandler() {

        var fixedClass = "header-fixed";
        var body = $("body");
        var logo = $("#logo img");
        var whiteLogo = logo.attr('src');
        var turquoiseLogo = "/assets/images/logo2.png";


        win.on("scroll", function () {

            if ($(this).scrollTop() > headerHeidth && !body.hasClass(fixedClass)) {
                body.addClass(fixedClass);
                logo.attr('src', turquoiseLogo);
            }

            if ($(this).scrollTop() > firstSectionHeight / 2 && body.hasClass(fixedClass)) {
                header.addClass('open');
            }

            if ($(this).scrollTop() <= firstSectionHeight / 2 && $(this).scrollTop() > headerHeidth && body.hasClass(fixedClass)) {
                header.removeClass('open');
            }

            if (($(this).scrollTop() <= headerHeidth) && body.hasClass(fixedClass)) {
                body.removeClass(fixedClass);
                logo.attr('src', whiteLogo);
            }
        });
    };

    win.on('resize', function () {
        winWidth = $(this).width();
        winHeight = $(this).height();
        mobile_menu_handler(menuContainer, winWidth, winHeight);

    });


    mobile_menu_handler(menuContainer, winWidth, winHeight);
    fixedMenuHandler();


    /******************************
     * DROPDOWN MENU
     *****************************/

    var dropDowmToggle = $('.dropdown-toggle');

    dropDowmToggle.on('click', function () {
        var el = $(this);
        el.parent('li.dropdown').toggleClass('open');
    });

    $('ul.dropdown-menu a').on('click', function () {
        dropDowmToggle.parent('li.dropdown').removeClass('open');
    });


    /*----------------------------
     SCROLLING
     -----------------------------*/
    var menuLinks = $(".section a[href^='#']");
    var main_menu_links = $('#main-menu a[href^="#"]');
    var sections = $("body > section, body > footer");
    var footermenuLinks = $('footer a[href*="#"]');


    function getSectionsPositions(obj) {

        var result = {};

        obj.each(function (e) {

            var section = $(this);
            var attrId = section.attr("id");
            var position = section.position().top;

            if (attrId != undefined && position != 0) {
                result[position] = "#" + attrId;
            }
        });
        return result;
    }

    function getKeys(obj) {
        var result = [];
        for (var propt in obj) {
            result.push(propt);
        }

        return result;
    }

    function scrollToSection(hash) {
        if ($(hash).length != 0) {
            $('html, body').animate({scrollTop: ($(hash).offset().top) - scrollCorrection}, 1000, 'easeInOutSine');
        }
    }

    var collection = getSectionsPositions(sections);
    var currScroll = 0;

    win.on("resize", function () {
        collection = getSectionsPositions(sections);
    });


    win.on("scroll", function () {
        currScroll = $(this).scrollTop();
        var keys = getKeys(collection);

        if (currScroll < headerHeidth)
            menuLinks.removeClass('active');

        keys.find(function (currentValue, index, arr) {

            if (currScroll + scrollCorrection >= currentValue) {
                menuLinks.removeClass('active');
                $("a[href^='" + collection[currentValue] + "']").addClass("active");
            }
        });
    });


    menuLinks.click(function () {
        var scroll_el = $(this).attr('href');
        menuLinks.removeClass('active');
        //main_menu_links.removeClass('active');
        scrollToSection(scroll_el);
        return false;
    });

    footermenuLinks.on('click', function () {
        var currLink = $(this);
        var hrefValue = currLink.attr('href');
        var idPosition = hrefValue.search("#");

        if (hrefValue.search("#") > -1) {
            var elID = hrefValue.substr(idPosition);
            scrollToSection(elID);
        }

    });

});
