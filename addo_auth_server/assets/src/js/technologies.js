$(document).ready(function () {

    function parseJsonData(data) {
        return $.parseJSON(data);
    }


    /***********************************
     *          Technologies
     **********************************/

    var technoSelector = "technologies";
    var technoContainer = $("#" + technoSelector + " .section__logo");
    var technoFilters = $("." + technoSelector + ".block-filter a");
    var technoSearchInput = $("#" + technoSelector + " .block-search input");
    var dataResponse = [];

    function getTechnoItemHtml(item, index = 0) {
        var result = '';

        result += '<div class="col-md-3 col-xs-4 logo-techno">';
        result += '<div class="logo-techno__img-box animated fadeInLeft wow" data-wow-delay="0.' + (index) + 's">';
        result += '<img src="/media/' + item.fields.image + '" alt="' + item.fields.title + '"  title="' + item.fields.title + '">';
        result += '</div></div>';

        return result;
    }

    function appendTechnoItems(data) {
        technoContainer.empty();
        data.forEach(function (item, i) {
            technoContainer.append(getTechnoItemHtml(item, i));
        })
    }

    function technoFilterHandler(el) {

        var filter = el.attr('data-filter');
        var order = el.attr('data-order');
        var result = [];


        if (typeof(filter) !== 'undefined' && filter !== "all") {

            result = dataResponse.filter(function (item) {
                return item.fields.tag.indexOf(parseInt(filter)) != -1;
            });
        }
        else if (filter == "all") {
            result = dataResponse;
        }

        if (typeof(order) !== 'undefined') {

            result = dataResponse;

            function comparisonForSorting(a1, b1, orderBy = 'asc') {

                if (a1 == b1) return 0;

                if(orderBy == 'desc')
                    return a1 < b1 ? 1 : -1;
                else
                    return a1 > b1 ? 1 : -1;
            }


            if (order == "title") {

                result.sort(function (a, b) {
                    var a1 = a.fields.title.toLowerCase(),
                        b1 = b.fields.title.toLowerCase();
                    return comparisonForSorting(a1, b1);
                });
            }

            if (order == "pk") {

                result.sort(function (a, b) {
                    return comparisonForSorting(a.pk, b.pk, 'desc');
                });
            }
        }

        return result;
    }

    function technoSearchHandler(search, data) {
        var result = [];

        result = data.filter(function (item) {
            var title = item.fields.title.toUpperCase();
            return title.indexOf(search.toUpperCase()) != -1;
        });

        return result;
    }

    function ajaxResponseHandler(callback) {
        $.ajax({
            type: 'GET',
            contentType: "application/json; charset=utf-8",
            url: '/technologies/as-json',
            dataType: "json",
            success: function (data) {
                callback(data);
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log(jqXHR);
                console.log(textStatus);
                console.log(errorThrown);

            }
        });
    }

    technoFilters.bind('click', function (el) {

        var e = $(this);

        function success() {
            technoFilters.removeClass('active');
            e.addClass('active');
            appendTechnoItems(technoFilterHandler(e, dataResponse));
        }

        if (dataResponse.length == 0)
            ajaxResponseHandler(function (data) {
                dataResponse = data;
                success();
            });
        else
            success();


    });

    technoSearchInput.keyup(function (e) {
        var searchValue = $(this).val();

        function success() {

            if (searchValue.length >= 2) {
                var result = technoSearchHandler(searchValue, dataResponse);
                appendTechnoItems(result);
            }


            if (searchValue.length == 0) {
                appendTechnoItems(dataResponse);
            }

            if (e.keyCode == 27) {
                appendTechnoItems(dataResponse);
                technoSearchInput.val('');
            }
        }

        if (dataResponse.length == 0)
            ajaxResponseHandler(function (data) {
                dataResponse = data;
                success();
            });
        else
            success();
    });


});
