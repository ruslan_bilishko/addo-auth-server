$(document).ready(function () {

    /********************************************************
     *          Notify Me
     *******************************************************/

    var signUpForm = $("#sign-up-form");
    var popupSelector = '#notify-me';

    function formPopup() {
        signUpForm.find("input, checkbox").attr('disabled', 'disabled');

        $.magnificPopup.open({
            items: {
                src: popupSelector
            }
        });
    }

    signUpForm.on("click", formPopup);

    $.LandingAjaxFormHandler('#notify-me-form', function () {
        $.magnificPopup.close({
            items: {
                src: popupSelector
            }
        });
    });




    /*******************************
     * LOGIN FORM HANDLER
     *******************************/
    //$.LandingAjaxFormHandler('#login-form');


});
