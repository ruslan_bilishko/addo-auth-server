$(document).ready(function () {

    /**
     * AJAX SETUP
     * @param name
     * @return {*}
     */

    //For getting CSRF token
    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?

                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!(/^http:.*/.test(settings.url) || /^https:.*/.test(settings.url))) {
                // Only send the token to relative URLs i.e. locally.
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });


    $.getDataForm = function (form) {
        var result = {};
        var data_form = form.serializeArray();

        data_form.forEach(function (item) {
            result[item.name] = item.value;
        });

        return result;
    };

    $.getMessages = function (data) {
        var message = '<ul class="messages__list">';

        data.forEach(function (mess) {

            message += '<li class="' + mess.status + '">';
            message += mess.message;
            message += '</li>';
        });

        message += '</ul>';

        return message;
    }


    $.LandingAjaxFormHandler = function (formSelector, callback=function () {

    }) {
        var formId = formSelector;
        var form = $(formId);
        var messages = $(".messages");
        var ajaxLoaderClass = 'ajax-loader';

        function inputErrorsHtml(data) {


            var result = '<ul class="errorlist">';

            data.forEach(function (item, i) {
                result += '<li>' + item.message + '</li>';
            });

            result += '</ul>';

            return result;
        }


        function errorsHandler(errors) {

            for (var prop in errors) {

                var field = $(formId + " input[name=" + prop + "], "
                    + formId + " textarea[name=" + prop + "]");

                field.addClass('has-errors').after(inputErrorsHtml(errors[prop]));
            }
        }

        function ajaxLoaderContainer() {

            var ajaxLoader = '<div class="' + ajaxLoaderClass + '"';
            ajaxLoader += '></div>';
            return ajaxLoader;
        }

        form.append(ajaxLoaderContainer());


        form.submit(function (e) {
            e.preventDefault();

            var currForm = $(this);
            var ajaxLoader = $('.'+ajaxLoaderClass);

            $(currForm).children(".has-errors").removeClass('has-errors');
            $(currForm).children('ul.errorlist').remove();
            messages.empty();


            $.ajax({
                type: currForm.attr('method'),
                url: currForm.attr('action'),
                data: $.getDataForm(currForm),
                contentType: 'application/x-www-form-urlencoded; charset=utf-8',  //Default
                processData: true,
                success: function (data) {
                    data = $.parseJSON(data);
                    form.find("input[type=text], textarea").val("");
                    messages.empty();

                    if (typeof(data.messages) !== 'undefined') {
                        messages.append($.getMessages(data.messages));
                    }

                    callback();
                },
                error: function (errors, status, errorThrown) {
                    errors = $.parseJSON(errors.responseJSON);

                    messages.empty();
                    if (typeof(errors.messages) !== 'undefined') {
                        messages.append($.getMessages(errors.messages));
                    }
                    errorsHandler(errors);
                },
                beforeSend: function () {
                    ajaxLoader.height(currForm.height()).show();

                },
                complete: function () {
                    ajaxLoader.height(1).hide();

                }
            });
        });
    };

});
