'use strict';

import gulp from 'gulp';
import del from 'del';
import loadPlugins from 'gulp-load-plugins';

let plugins = loadPlugins();

const client = './addo_auth_server';
const assetsFolder = 'assets';
const assets = `${client}/${assetsFolder}`;

let path = {
    template: `${client}/landing/templates/landing/layout`,
    index_html: `${client}/landing/templates/landing/layout/base.html`,
    dist: './dist',
    bowerComponents: './bower_components/',
    build: {
        root: `${assets}/build/`,
        css: `${assets}/build/css/`,
        js: `${assets}/build/js/`,
        libraries: `${assets}/libraries`,
        fonts: `${assets}/fonts`,
        images: `${assets}/images/`,
        bower: `${client}/build/bower_components`,
    },
    src: {
        js: `${assets}/src/js/**/*.js`,
        style: `${assets}/src/style/main.scss`,
        libs: 'bower_components/'
    },
    img: {
        images: `${assets}/images/**/*`,
    }
};

let config = {
    files: {
        vendorJs: "vendor.js",
        appJs: "app.js",
        vendorCss: "vendor.css",
        appCss: "app.css"
    }
};

let PathBowerFiles = `${path.dist}/libs`;

/* Clean build */
gulp.task('clean', () => {
    return del([
        path.build.css,
        path.build.js,
        path.build.fonts,
        path.dist
    ]);
});

/*Compress*/

gulp.task('compress:images', () => {
    gulp.src(path.img.images)
        .pipe(plugins.cache(plugins.imagemin({verbose: true})))
        .pipe(gulp.dest(path.build.images))
});

/*Copy*/

gulp.task('copy:fonts', () => {

    let pathToFonts = [
        `${path.bowerComponents}/elegant-icons/**/*`,
        `${path.bowerComponents}/font-awesome/**/*`,
        `${path.bowerComponents}/fonts-raleway/**/*`,
        `${path.bowerComponents}/roboto-fontface/**/*`,
    ];

    return gulp.src(pathToFonts, {base: path.bowerComponents})
        .pipe(gulp.dest(path.build.fonts));
});

gulp.task('copy:bootstrap-fonts', () => {
    return gulp.src(
        `${path.bowerComponents}/bootstrap/fonts/**/*`,
        {
            base: `${path.bowerComponents}/bootstrap`
        }
    ).pipe(gulp.dest(`${path.build.fonts}/bootstrap`));
});

gulp.task('copy:vendors', () => {
    return gulp.src('./bower.json')
        .pipe(plugins.mainBowerFiles({
            overrides: {
                bootstrap: {
                    main: [
                        './dist/css/bootstrap.css',
                        './dist/js/bootstrap.js'
                    ],
                },
                "elegant-icons": {ignore: true},
                "font-awesome": {ignore: true},
                "fonts-raleway": {ignore: true},
                "roboto-fontface": {ignore: true},
            }
        }))
        .pipe(gulp.dest(`${PathBowerFiles}`));
});

gulp.task('copy:animo', () => {
    gulp.src([
        `${path.bowerComponents}/animo/**/*.css`,
        `!${path.bowerComponents}/animo/**/*.min.css`,
    ])
    .pipe(plugins.flatten({includeParents: 1}))
    .pipe(gulp.dest(`${PathBowerFiles}/animo`));
});

/*Replace*/

gulp.task('replace:bootstrap-fonts', () => {
    return gulp.src(`${PathBowerFiles}/bootstrap/dist/css/bootstrap.css`)
        .pipe(plugins.replace('../fonts/', `/assets/fonts/bootstrap/fonts/`))
        .pipe(gulp.dest(`${PathBowerFiles}/bootstrap/dist/css`));
});

/* Build vendor */

gulp.task('build:vendor:scripts', () => {
    return gulp.src(`${path.dist}/libs/**/*.js`)
        .pipe(plugins.order([
            `jquery/dist/jquery.js`,
            `bootstrap/dist/js/bootstrap.js`
        ]))
        .pipe(plugins.minify({
            ext: {
                min: '.js'
            },
            noSource: true
        }))
        .pipe(plugins.concat(config.files.vendorJs))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('build:vendor:styles', () => {

    return gulp.src(`${path.dist}/libs/**/*.css`)
        .pipe(plugins.order([
            `bootstrap/dist/css/bootstrap.css`
        ]))
        .pipe(plugins.cleanCss({
          level: {
            1: {
              specialComments: false
            }
          }
        }))
        .pipe(plugins.concat(config.files.vendorCss))
        .pipe(gulp.dest(path.build.css));
});

/* Build app*/

gulp.task('build:app:scripts', () => {
    return gulp.src(path.src.js)
        .pipe(plugins.minify({
            ext: {
                min: '.js'
            },
            noSource: true
        }))
        .pipe(plugins.concat(config.files.appJs))
        .pipe(gulp.dest(path.build.js));
});

gulp.task('build:app:styles', () => {
    return gulp.src(path.src.style)
        .pipe(plugins.sass())
        .pipe(plugins.autoprefixer())
        .pipe(plugins.cleanCss())
        .pipe(plugins.concat(config.files.appCss))
        .pipe(gulp.dest(path.build.css));
});

/*Build*/

gulp.task('bootstrap-fonts', plugins.sequence(
    'copy:bootstrap-fonts',
    'replace:bootstrap-fonts'
));

gulp.task('build:app', plugins.sequence(
    'build:app:scripts',
    'build:app:styles',
));

gulp.task('build:vendor', plugins.sequence(
    'build:vendor:scripts',
    'build:vendor:styles',
));

gulp.task('build', ['clean'], plugins.sequence(
    'copy:vendors',
    'copy:animo',
    'copy:fonts',
    'bootstrap-fonts',
    'compress:images',
    'build:vendor',
    'build:app'
));

/*Watch*/

gulp.task('watch', ['build'], () => {
  gulp.watch(`${assets}/src/style/**/*.scss`, ['build:app:styles']);
  gulp.watch(`${assets}/src/js/**/*.js`, ['build:app:scripts']);
});
